<?php

namespace DatabaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BarbacoaPhoto
 *
 * @ORM\Table(name="barbacoa_photo", indexes={@ORM\Index(name="fk_barbacoa_photo_barbacoa", columns={"barbacoa_id"})})
 * @ORM\Entity
 */
class BarbacoaPhoto
{
    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255, nullable=false)
     */
    private $path;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DatabaseBundle\Entity\Barbacoa
     *
     * @ORM\ManyToOne(targetEntity="DatabaseBundle\Entity\Barbacoa", inversedBy="barbacoa_photos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="barbacoa_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $barbacoa;



    /**
     * Set path
     *
     * @param string $path
     *
     * @return BarbacoaPhoto
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return BarbacoaPhoto
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return BarbacoaPhoto
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set barbacoa
     *
     * @param \DatabaseBundle\Entity\Barbacoa $barbacoa
     *
     * @return BarbacoaPhoto
     */
    public function setBarbacoa(\DatabaseBundle\Entity\Barbacoa $barbacoa)
    {
        $this->barbacoa = $barbacoa;

        return $this;
    }

    /**
     * Get barbacoa
     *
     * @return \DatabaseBundle\Entity\Barbacoa
     */
    public function getBarbacoa()
    {
        return $this->barbacoa;
    }
}
