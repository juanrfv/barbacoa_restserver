<?php

namespace DatabaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rental
 *
 * @ORM\Table(name="rental", indexes={@ORM\Index(name="fk_rental_status", columns={"rental_status_id"})})
 * @ORM\Entity
 */
class Rental
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DatabaseBundle\Entity\Barbacoa
     *
     * @ORM\ManyToOne(targetEntity="DatabaseBundle\Entity\Barbacoa", inversedBy="rentals")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="barbacoa_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $barbacoa;

    /**
     * @var \DatabaseBundle\Entity\RentalStatus
     *
     * @ORM\ManyToOne(targetEntity="DatabaseBundle\Entity\RentalStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="rental_status_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $rental_status;



    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Rental
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Rental
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Rental
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set barbacoa
     *
     * @param \DatabaseBundle\Entity\Barbacoa $barbacoa
     *
     * @return Rental
     */
    public function setBarbacoa(\DatabaseBundle\Entity\Barbacoa $barbacoa)
    {
        $this->barbacoa = $barbacoa;

        return $this;
    }

    /**
     * Get barbacoa
     *
     * @return \DatabaseBundle\Entity\Barbacoa
     */
    public function getBarbacoa()
    {
        return $this->barbacoa;
    }

    /**
     * Set rentalStatus
     *
     * @param \DatabaseBundle\Entity\RentalStatus $rentalStatus
     *
     * @return Rental
     */
    public function setRentalStatus(\DatabaseBundle\Entity\RentalStatus $rentalStatus)
    {
        $this->rental_status = $rentalStatus;

        return $this;
    }

    /**
     * Get rentalStatus
     *
     * @return \DatabaseBundle\Entity\RentalStatus
     */
    public function getRentalStatus()
    {
        return $this->rental_status;
    }
}
