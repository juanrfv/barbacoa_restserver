<?php

namespace DatabaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Barbacoa
 *
 * @ORM\Table(name="barbacoa", indexes={@ORM\Index(name="fk_barbacoa_user", columns={"user_id"})})
 * @ORM\Entity
 */
class Barbacoa
{
    /**
     * @var string
     *
     * @ORM\Column(name="model", type="string", length=255, nullable=false)
     */
    private $model;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="operative", type="boolean", nullable=false)
     */
    private $operative = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="DatabaseBundle\Entity\BarbacoaPhoto", mappedBy="barbacoa", cascade={"remove","refresh"})
     */
    private $barbacoa_photos;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="DatabaseBundle\Entity\Rental", mappedBy="barbacoa", cascade={"remove","refresh"})
     */
    private $rentals;

    /**
     * @var \DatabaseBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="DatabaseBundle\Entity\User", inversedBy="barbacoas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->barbacoa_photos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->rentals = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set model
     *
     * @param string $model
     *
     * @return Barbacoa
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get model
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Barbacoa
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set operative
     *
     * @param boolean $operative
     *
     * @return Barbacoa
     */
    public function setOperative($operative)
    {
        $this->operative = $operative;

        return $this;
    }

    /**
     * Get operative
     *
     * @return boolean
     */
    public function getOperative()
    {
        return $this->operative;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Barbacoa
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Barbacoa
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add barbacoaPhoto
     *
     * @param \DatabaseBundle\Entity\BarbacoaPhoto $barbacoaPhoto
     *
     * @return Barbacoa
     */
    public function addBarbacoaPhoto(\DatabaseBundle\Entity\BarbacoaPhoto $barbacoaPhoto)
    {
        $this->barbacoa_photos[] = $barbacoaPhoto;

        return $this;
    }

    /**
     * Remove barbacoaPhoto
     *
     * @param \DatabaseBundle\Entity\BarbacoaPhoto $barbacoaPhoto
     */
    public function removeBarbacoaPhoto(\DatabaseBundle\Entity\BarbacoaPhoto $barbacoaPhoto)
    {
        $this->barbacoa_photos->removeElement($barbacoaPhoto);
    }

    /**
     * Get barbacoaPhotos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBarbacoaPhotos()
    {
        return $this->barbacoa_photos;
    }

    /**
     * Add rental
     *
     * @param \DatabaseBundle\Entity\Rental $rental
     *
     * @return Barbacoa
     */
    public function addRental(\DatabaseBundle\Entity\Rental $rental)
    {
        $this->rentals[] = $rental;

        return $this;
    }

    /**
     * Remove rental
     *
     * @param \DatabaseBundle\Entity\Rental $rental
     */
    public function removeRental(\DatabaseBundle\Entity\Rental $rental)
    {
        $this->rentals->removeElement($rental);
    }

    /**
     * Get rentals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRentals()
    {
        return $this->rentals;
    }

    /**
     * Set user
     *
     * @param \DatabaseBundle\Entity\User $user
     *
     * @return Barbacoa
     */
    public function setUser(\DatabaseBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \DatabaseBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
