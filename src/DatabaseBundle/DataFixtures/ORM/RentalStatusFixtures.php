<?php

namespace DatabaseBundle\DataFixtures\ORM;

use DatabaseBundle\Entity\RentalStatus;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class RentalStatusFixtures extends Fixture {
  
  public function load(ObjectManager $manager){
    $array =array("Reserved","Approved","Cancelled", "Finished");
    foreach($array as $status){
      $record = new RentalStatus();
      $record->setDescription($status);
      $manager->persist($record);
    }
    $manager->flush();
  }

}