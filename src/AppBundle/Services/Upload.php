<?php
namespace AppBundle\Services;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Upload{
    private $container;
    private $targetDirectory;
    private $folder;
    private $folderLogo;

    public function __construct(Container $container){
        $this->container        =  $container;
        $this->targetDirectory  = $this->container->getParameter('kernel.root_dir') . '/../web/';
        $this->folder           = 'uploads/' . date('Y') . "/" . date('m');
        $this->folderLogo       = 'uploads/logos/';
        $this->folderLogoPreview= 'uploads/logos_preview/';
        $this->folderMultimedia = 'uploads/multimedia/';
        $this->folderMultimediaP= 'uploads/multimedia_preview/';
    }

    public function serialAleatory($longitud){
        $key     = '' ;
        $pattern = '1234567890' ;
        $max = strlen ( $pattern ) - 1 ;
        for ( $i = 0 ; $i < $longitud ; $i ++ ){
            $key .= $pattern{mt_rand ( 0 , $max )} ;
        }
        return $key ;
    }

    public function ak_img_resize($target, $newcopy, $w, $h, $ext){
        list($w_orig, $h_orig) = @getimagesize($target);
        $h_orig = $h_orig > 0 ? $h_orig : 1;
        $scale_ratio = $w_orig / $h_orig; //  altezza / larguezza
        if (($w / $h) > $scale_ratio) {  // largueza / altezza > scala
            $w = $h * $scale_ratio;
        } else {
            $h = $w / $scale_ratio;
        }
        $img = "";
        $ext = strtolower($ext);
        $file_name_extecion = explode('.', $ext);
        $ext = isset($file_name_extecion[1]) ? $file_name_extecion[1] : "";
        if ($ext != ""){
            if ($ext == "gif") {
                $img = @imagecreatefromgif($target);
            } else if ($ext == "png") {
                $img = @imagecreatefrompng($target);
            } else {
                $img = @imagecreatefromjpeg($target);
            }
            $tci = imagecreatetruecolor($w, $h);
            @imagecopyresampled($tci, $img, 0, 0, 0, 0, $w, $h, $w_orig, $h_orig);
            @imagejpeg($tci, $newcopy, 80);
        }
    }

    public function uploadLogo(UploadedFile $file, $id, $nombre,$ancho =150, $alto = 150){
        $dirLogo         = $this->targetDirectory.$this->folderLogo;
        $dirLogoPreview  = $this->targetDirectory.$this->folderLogoPreview;
        
        $ext = $file->guessExtension();
        if ($this->validateExtension($ext)) {
            $nom = $file->getClientOriginalName();
            if (($nombre != "nuevo") && ($id != "nuevo")) {
                $fileName = $id."_".date('is')."_".$nombre.".".$ext;
            } else {
                $fileName = date('Y')."_".date('m')."_".date('d')."_".$nom;
            }
            $file->move($this->targetDirectory . $this->folderLogo, "tmp_".$fileName);
            
            $this->ak_img_resize($dirLogo."tmp_".$fileName, $dirLogo.$fileName, $ancho, $alto, $fileName);
            $this->ak_img_resize($dirLogo."tmp_".$fileName, $dirLogoPreview.$fileName, 80, 80, $fileName);
            unlink($dirLogo."tmp_".$fileName);
            
            $response = array(
                'name' => $fileName,
                'response' => true
            );
        } else {
            $response = array(
                'data' => 'Extension no valida, solo imagenes',
                'response' => false
            );
        }
        return $response;
    }

    public function upload(UploadedFile $file, $prefijo, $ancho =320, $alto = 240){
        $dirMultimedia         = $this->targetDirectory.$this->folderMultimedia;
        $dirMultimediaPreview  = $this->targetDirectory.$this->folderMultimediaP;
        
        $ext = $file->guessExtension();
        if ($this->validateExtension($ext)) {
            $fileName = $prefijo.$this->serialAleatory(4).".".$ext;
            $file->move($dirMultimedia, "tmp_".$fileName);

            $this->ak_img_resize($dirMultimedia."tmp_".$fileName, $dirMultimedia.$fileName, $ancho, $alto, $fileName);
            $this->ak_img_resize($dirMultimedia."tmp_".$fileName, $dirMultimediaPreview."preview_".$fileName, 220, 190, $fileName);
            unlink($dirMultimedia."tmp_".$fileName);
            
            $response = array(
                'name' => $fileName,
                'response' => true
            );
        } else {
            $response = array(
                'data' => 'Extension no valida, solo imagenes',
                'response' => false
            );
        }
        return $response;
    }

    public function validateExtension($ext = null){
        $extvalidas = array("jpeg", "jpg", "png", "gif", "bmp");
        return in_array($ext, $extvalidas);
    }

    public function setFile(UploadedFile $file = null){
        $this->file = $file;
        if (isset($this->path)) {
            $this->temp = $this->path;
            $this->path = null;
        } else {
            $this->path = 'initial';
        }
    }

    public function preUpload(){
        if (null !== $this->getFile()) {
            $filename = sha1(uniqid(mt_rand(), true));
            $this->path = $filename . '.' . $this->getFile()->guessExtension();
        }
    }

    public function removeUpload($file){
        $filename     = $this->targetDirectory . '/uploads/multimedia/' . $file;
        $filenamePrev = $this->targetDirectory . '/uploads/multimedia_preview/preview_' . $file;
        if (file_exists($filename) && file_exists($filenamePrev)) {
            unlink($filename);
            unlink($filenamePrev);
            return true;
        } else {
            return false;
        }
    }

    public function removeLogo($file){
        $filename     = $this->targetDirectory . '/uploads/logos/' . $file;
        $filenamePrev = $this->targetDirectory . '/uploads/logos_preview/preview_' . $file;
        if (file_exists($filename) && file_exists($filenamePrev)) {
            unlink($filename);
            unlink($filenamePrev);
            return true;
        } else {
            return false;
        }
    }
}