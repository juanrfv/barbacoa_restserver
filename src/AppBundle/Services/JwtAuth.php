<?php
    namespace AppBundle\Services;

    use Doctrine\ORM\EntityManager;
    use Firebase\JWT\JWT;


    class JwtAuth
    {
        public $em;
        public $key;
        public $id_admin;
        public $view = false;


        public function __construct(EntityManager $em)
        {
            $this->em = $em;
            $this->key = "clave-secreta";
            $this->idAdmin = 3;
            $this->idNew = 2;
        }


        // COMPRUEBA SI EXISTE USUARIO
        // RETORNA DATOS CODIFICADOS (TOKEN) HO DATOS LIMPIOS OBJ
        public function signup($email, $password, $getHash = null)
        {
            $key = $this->key;            
            $user = $this->em->getRepository('BackendBundle:Usuario')->findOneBy(array("email" => $email, "password" => $password));// buscar usuario
            
            // si existe el usuario creamos el token
            is_object($user) ? $signup = true : $signup = false;
            if ($signup == true)
            {
                $token = array( "sub"     => $user->getId(), // identificador del usuario
                                "role"    => $user->getRole(),
                                "email"    => $user->getEmail(),
                                "name"     => $user->getName(),
                                "surname"  => $user->getSurname(),
                                "telefono" => $user->getTelefono(),
                                "password" => $user->getPassword(),
                                'image'    => $user->getMediaId(),
                                "iat"      => time(), // cuando se crea el token
                                "exp"      => time() + (15 * 24 * 60 * 60) ); // fecha de expiracion - suma una semana a time()

                $jwt = JWT::encode($token, $key, 'HS256'); // codifica array de datos y devuelve un TOKEN - HASH (datos cifrados )
                $decode = JWT::decode($jwt, $key, array( 'HS256' ));// decodifica los datos (datos limpios obj )

                if ($getHash != null)//si piden el hash retornamos TOKEN
                    return $jwt;
                else
                    return $decode; //sino retornamos datos limpios obj
            } else
                return array("status" => "error", "data" => "El email o la contraseña no son correctos");
//                return array( "status" => "error", "data" => "login fallido", "pass"=> $password, "email"=> $email);
        }





        // CHEKEA SI EL TOKEN  ES CORRECTO
        // RETORNA BOLEAN HO DATOS DECODIFICADOS
        public function checkToken($jwt, $getIdentity = false)
        {
            $key = $this->key;
            $auth = false;
            $decode = false;

            // capturando excepciones
            try{
                $decode = JWT::decode($jwt, $key, array( 'HS256' )); // decodificamos el token
            } catch (\UnexpectedValueException $e){
                $auth = false;
            } catch (\DomainException $e){
                $auth = false;
            }

            // si existe la propiedad sub (id usuario) el token es correcto
            if (isset($decode->sub))
                $auth = true;
            else
                $auth = false;

            if ($getIdentity == true)
                return $decode;
            else
                return $auth;
        }

        public function checkRole($hash)
        {
            $identity = $this->checkToken($hash, true);  //  decodificar token
            if ($identity->role == $this->idAdmin) {
                $this->view = true;
                return true;
            }
            return false;
        }


        public function checkRoleNew($hash)
        {
            $identity = $this->checkToken($hash, true);  //  decodificar token
            if ($identity->role == $this->idNew) {
                $this->view = true;
                return true;
            }
            return false;
        }


        public function viewPass(){
            return $this->view;
        }



    }