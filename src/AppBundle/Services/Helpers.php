<?php
namespace AppBundle\Services;

//LIBRERIAS CONVERTIR A JSON
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

class Helpers{
    public $em;

    public function __construct(EntityManager $em){
        $this->em = $em;
    }

    // RETORNA DATOS EN JSON
    public function json($data){
        $normalizers = new GetSetMethodNormalizer();
        $normalizers->setCircularReferenceLimit(0);
        $normalizers->setCircularReferenceHandler(function ($object) {});
        $encoder = new JsonEncoder();

        $callback = function ($dateTime) {
            return $dateTime instanceof \DateTime
                ? $dateTime->format('Y-m-d')
                : '';
        };

        $callback_hora = function ($dateTime) {
            return $dateTime instanceof \DateTime
                ? $dateTime->format('H:i:s')
                : '';
        };

        $callback_email = function ($email) {
            return null;
        };

        $normalizers->setCallbacks(
            [
                'createdAt'   => $callback,
                'updatedAt'   => $callback,
                'fechaFinal'  => $callback,
                'fechaInizio' => $callback,
                'fechaSelect' => $callback,
                'horaSelect'  => $callback_hora,
                'email'       => $callback_email,
                'password'    => $callback_email
            ]
        );

        $serializer = new Serializer(array($normalizers), array($encoder));
        $json = $serializer->serialize($data, 'json');

        $response = new Response();
        $response->setContent($json);
        $response->headers->set("Content-Type", "application/json");

        return $response;
    }

    public function JsonSinCallbacksEmail($data){
        $normalizers = new GetSetMethodNormalizer();
        $encoder = new JsonEncoder();

        $callback = function ($dateTime) {
            return $dateTime instanceof \DateTime
                ? $dateTime->format('d/m/Y')
                : '';
        };

        $callback_hora = function ($dateTime) {
            return $dateTime instanceof \DateTime
                ? $dateTime->format('H:i:s')
                : '';
        };

        $callback_password = function ($email) {
            return null;
        };

                $normalizers->setCallbacks(
            [
                'createdAt'   => $callback,
                'updatedAt'   => $callback,
                'fechaFinal'  => $callback,
                'fechaInizio' => $callback,
                'fechaSelect' => $callback,
                'horaSelect'  => $callback_hora,
                'password'    => $callback_password
            ]
        );


        $serializer = new Serializer(array($normalizers), array($encoder));
        $json = $serializer->serialize($data, 'json');

        $response = new Response();
        $response->setContent($json);
        $response->headers->set("Content-Type", "application/json");

        
        return $response;
    
    }

    // SIRVE para tener checkToken() DEL SERVIZIO JwtAuth DENTRO DEL HELPER
    // RETORNA BOLEAN HO DATOS DECODIFICADOS
    public function authCheck($hash, $getIdentity = false)
    {
        $jwt_auth = $this->jwt_auth;

        $auth = false;
        if ($hash != null) { // si $hash no esta vacio
            if ($getIdentity == false) {  // si $getIdentity == false retorna bolean

                $check_token = $jwt_auth->checkToken($hash);
                if ($check_token == true)
                    $auth = true;

            } else { //sino retorna datos decodificados
                $check_token = $jwt_auth->checkToken($hash, true);
                if (is_object($check_token))
                    $auth = $check_token;
            }
        }
        return $auth;
    }


    public function log(){
//        error_log("{$this->request}\n", 3, "./data/{$this->logFileName}.log");
    }

    public function slugify($string){
        $string = strtolower($string);
        $patron = array (
            // Espacios, puntos y comas por guion
            '/[\., ]+/' => '-',
            // Vocales
            '/\+/' => '',
            '/"/' => '',
            "/'/" => '',
            '/á/' => 'a',
            '/é/' => 'e',
            '/í/' => 'i',
            '/ó/' => 'o',
            '/ú/' => 'u',
            '/â/' => 'a',
            '/ê/' => 'e',
            '/î/' => 'i',
            '/ô/' => 'o',
            '/û/' => 'u',
            '/à/' => 'a',
            '/è/' => 'e',
            '/ì/' => 'i',
            '/ò/' => 'o',
            '/ù/' => 'u',
            '/ä/' => 'a',
            '/ë/' => 'e',
            '/ï/' => 'i',
            '/ö/' => 'o',
            '/ü/' => 'u',
            '/&agrave;/' => 'a',
            '/&egrave;/' => 'e',
            '/&igrave;/' => 'i',
            '/&ograve;/' => 'o',
            '/&ugrave;/' => 'u',
 
            '/&aacute;/' => 'a',
            '/&eacute;/' => 'e',
            '/&iacute;/' => 'i',
            '/&oacute;/' => 'o',
            '/&uacute;/' => 'u',
 
            '/&acirc;/' => 'a',
            '/&ecirc;/' => 'e',
            '/&icirc;/' => 'i',
            '/&ocirc;/' => 'o',
            '/&ucirc;/' => 'u',
 
            '/&atilde;/' => 'a',
            '/&etilde;/' => 'e',
            '/&itilde;/' => 'i',
            '/&otilde;/' => 'o',
            '/&utilde;/' => 'u',
 
            '/&auml;/' => 'a',
            '/&euml;/' => 'e',
            '/&iuml;/' => 'i',
            '/&ouml;/' => 'o',
            '/&uuml;/' => 'u',
 
            // Otras letras y caracteres especiales
            '/&aring;/' => 'a',
            '/&ntilde;/' => 'n',
            '/ñ/' => 'n',
            '/&/' => 'y',
            '/--/' => '-',
            //'/[^a-z0-9]/'=>'-',
        );
        return $string = preg_replace(array_keys($patron),array_values($patron),strtolower(trim(strip_tags($string))));
    }

}